var current_bpm = document.getElementById("#current_bpm");
var next_track_bpm = document.getElementById('#next_track_bpm');
var bpm_diff;

function Validate() {
    $('#error p').remove();

    // store the error div, to save typing
    var error = $('#error');

    // we start by assuming everything is correct
    // if it later turns out not to be, we just set
    // this to false
    var correct = true;

    //error message if input is black
    if ($('#current_bpm').val() == '') {
        error.append('<p>Current bpm not provided</p>');
        correct = false;
    }

    // same
    if ($('#next_track_bpm').val() == '') {
        error.append('<p>Next track bpm not provided</p>');
        correct = false;
    }
    // if we haven't found an error, we hide the error message
    if (correct) {
        error.css('display', 'none');

        // clear the result div
        $('#result').empty();
        // and put together a result message
        $('#result').append('<p>Your two bpms are ' + $('#current_bpm').val()  + ' and' + ' '  + $('#next_track_bpm').val() + '</p>');
    }
    // otherwise, we show the error
    else {	
        error.css('display', 'block');
    }

    return false;
}

function compare_bpms(){
    if (current_bpm > next_track_bpm) {
        var bpm_diff = current_bpm - next_track_bpm
    } else {
        alert('test');
    }
};

$('#contact').submit(Validate);
